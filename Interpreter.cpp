#include <iostream>
#include <string>
using namespace std;

class Expression;
class terminalExpression;

class andExpression;
class orExpression;

class Expression {
public:
	Expression() {}
	virtual bool interpret(string context) {
		cout << " SHOULD NOT BE CALLED " << endl;
		return true;
	}
};

class terminalExpression : public Expression {

	string data;

public:
	terminalExpression(string data) {
		this->data = data;
	}

	bool interpret(string context) {
		return context.find(data)!=string::npos;
	}
};

class andExpression : public Expression {

	Expression * e1;
	Expression * e2;

public:
	andExpression(Expression * e1, Expression * e2) {
		this->e1 = e1;
		this->e2 = e2;
	}

	bool interpret(string context) {
		return e1->interpret(context) && e2->interpret(context);
	}

};

class orExpression : public Expression {

	Expression * e1;
	Expression * e2;

public:
	orExpression(Expression * e1, Expression * e2) {
		this->e1 = e1;
		this->e2 = e2;
	}

	bool interpret(string context) {
		return e1->interpret(context) || e2->interpret(context);
	}
};


int main() {
	//actions

		// run
		terminalExpression run("run");
		terminalExpression flee("flee");
		orExpression runAway(&run, &flee);


		// attack
		terminalExpression attack("attack");
		terminalExpression hit("hit");
		terminalExpression bash("bash");
		terminalExpression destroy("destroy");
		terminalExpression cut("cut");
		terminalExpression stab("stab");
		terminalExpression rek("rek");
		terminalExpression kill("kill");
		orExpression att1(&attack, &hit);
		orExpression att2(&att1, &bash);
		orExpression att3(&att2, &destroy);
		orExpression att4(&att3, &cut);
		orExpression att5(&att4, &stab);
		orExpression att6(&att5, &rek);
		orExpression violentApproach(&att6, &kill);

		//dodge
		terminalExpression dodge("dodge");

	//target
		// head
		terminalExpression head("head");
		terminalExpression eyes("eyes");
		terminalExpression mouth("mouth");
		orExpression head1(&head, &eyes);
		orExpression targetHead(&head1, &mouth);

		// heart
		terminalExpression heart("heart");
		terminalExpression chest("chest");
		orExpression targetHeart(&heart, &chest);

		//yourself
		terminalExpression myself("myself");
		terminalExpression suicide("suicide");
		andExpression suic1(&myself, &violentApproach);
		orExpression suicidalThoughts(&suic1, &suicide);

	//combination
		andExpression attackHead(&violentApproach, &targetHead);
		andExpression attackHeart(&violentApproach, &targetHeart);


	while (true) {
		cout << "There's a giant monster in front of you. What do you do?" << endl;
		string response;
		getline(cin, response);

		if (suicidalThoughts.interpret(response)) {
			cout << "Taking the easy way out, you kill yourself before the monster can kill you.  You die" << endl;
		} else if (runAway.interpret(response)) {
			cout << "The monster, too agile for you, catches you and you die" << endl;
		}else if (dodge.interpret(response)) {
			cout << "You fail to dodge and you die" << endl;
		}else if (attackHead.interpret(response)) {
			cout << "You aim for its head and somehow it works. Congratulations" << endl;
		}else if (attackHeart.interpret(response)) {
			cout << "You aim for its heart, but it's too well guarded. You die." << endl;
		}else if (violentApproach.interpret(response)) {
			cout << "You flail at the monster aimlessly and you die." << endl;
		}
		else {
			cout << "While wondering what the voice in your head is saying, you die." << endl;
		}


		system("pause");
		system("cls");
	}
	return 0;
}